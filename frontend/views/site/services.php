<section class="home-slider owl-carousel">
    <div class="slider-item bread-item" style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container" data-scrollax-parent="true">
            <div class="row slider-text align-items-end">
                <div class="col-md-7 col-sm-12 ftco-animate mb-5">
<!--                    <p class="breadcrumbs" data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span class="mr-2"><a href="#">Home</a></span> <span>Services</span></p>-->
                    <h1 class="mb-3" data-scrollax=" properties: { translateY: '70%', opacity: .9}">Our Dental Service Keeps you Smile</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-2">Our Dental Service Keeps you Smile</h2>
                <p>"A smile is the prettiest thing you can wear" and we work towards to get you that with our quality Dental treatment.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-tooth"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Teeth Whitening</h3>
                        <p>Whitening is among the most popular dental procedures because it can greatly improve how your teeth look.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-dental-care"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Teeth Cleaning</h3>
                        <p>Teeth Cleaning is part of oral hygiene and involves the removal of dental plaque from teeth (Dental caries).</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-tooth-with-braces"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Quality Brackets</h3>
                        <p>Orthodontics is the branch of dentistry that corrects teeth and jaws that are positioned improperly.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-anesthesia"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Modern Anesthetic</h3>
                        <p>Modern Anesthetic is a field of anesthesia that includes not only local but sedation and general anesthesia.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-dental-care"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Dental Calculus</h3>
                        <p>Brushing and flossing can remove plaque from which calculus forms; however, once formed, calculus is too hard (firmly attached) to be removed with a toothbrush. Calculus buildup can be removed with ultrasonic tools or dental hand instruments (such as a periodontal scaler).</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-bacteria"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Paradontosis</h3>
                        <p>Periodontitis (formerly known as parodontosis) is a progressive gingival and periodontal tissue inflammation disease. This is the most common cause of tooth loss in subjects above 35 years of age.</p>
                            <p>Periodontitis, or gum disease, is a common infection that damages the soft tissue and bone supporting the tooth. Without treatment, the alveolar bone around the teeth is slowly and progressively lost. The main aim of treatment is to clean out bacteria from the pockets around the teeth and prevent further destruction of bone and tissue.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-dentist"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Dental Implants</h3>
                        <p>A dental implant (also known as an endosseous implant or fixture) is a surgical component that interfaces with the bone of the jaw or skull to support a dental prosthesis such as a crown, bridge, denture, facial prosthesis or to act as an orthodontic anchor. The basis for modern dental implants is a biologic process called osseointegration, in which materials such as titanium form an intimate bond to bone. The implant fixture is first placed so that it is likely to osseointegrate, then a dental prosthetic is added. A variable amount of healing time is required for osseointegration before either the dental prosthetic (a tooth, bridge or denture) is attached to the implant or an abutment is placed which will hold a dental prosthetic.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-dental-care-1"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Tooth Braces</h3>
                        <p>Dental braces are devices used in orthodontics that align and straighten teeth and help position them with regard to a person's bite, while also aiming to improve dental health.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>