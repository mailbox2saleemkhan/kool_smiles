<section class="home-slider owl-carousel">
    <div class="slider-item" style="background-image: url('<?= getenv('FRONTEND_URL'); ?>/img/3.jpg');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text align-items-center" data-scrollax-parent="true">
                <div class="col-md-6 col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                    <!--                    <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Modern Dentistry in-->
                    <!--                        a Calm and Relaxed Environment</h1>-->
                    <!--                                        <p class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">A small river named-->
                    <!--                                            Duden flows by their place and supplies it with the necessary regelialia.</p>-->
                    <!--                                        <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">-->
                    <!--                                            <a href="#"-->
                    <!--                                                                                                              class="btn btn-primary px-4 py-3">Make-->
                    <!--                                                an Appointment</a></p>-->
                </div>
            </div>
        </div>
    </div>
    <div class="slider-item" style="background-image: url('<?= getenv('FRONTEND_URL'); ?>/img/1.jpg');">
        <div class="overlay"></div>

    </div>
    <div class="slider-item" style="background-image: url('<?= getenv('FRONTEND_URL'); ?>/img/4.jpg');">
        <div class="overlay"></div>

    </div>
    <div class="slider-item" style="background-image: url('<?= getenv('FRONTEND_URL'); ?>/img/header-img.jpg');">
        <div class="overlay"></div>
    </div>

</section>
<section class="ftco-intro">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-6 color-1 p-4">
                <h1 class="mb-4">KOOL SMILES DENTAL - Best Dentist - Dental Surgeon In Greater Noida</h1>
                <p>KOOL SMILES DENTAL offers more than what you might expect from the best dental clinic in Noida &
                    Greater Noida. The Dental Surgeon at the clinic is considered the best dentist in Greater Noida and
                    does painless Root Canal Treatments.</p>

            </div>
            <div class="col-md-6 color-2 p-4">
                <h3 class="mb-4">Opening Hours</h3>
                <p class="openinghours d-flex">
                    <span>Morning Time :</span>
                    <span>09:00 AM - 01:00 PM</span>
                </p>
                <p class="openinghours d-flex">
                    <span>Evening Time :</span>
                    <span>05:00 PM - 09:00 PM</span>
                </p>
            </div>
            <!--            <div class="col-md-6 color-3 p-4">-->
            <!--                <h3 class="mb-2">Make an Appointment</h3>-->
            <!--                <form action="#" class="appointment-form">-->
            <!--                    <div class="row">-->
            <!--                        <div class="col-sm-4">-->
            <!--                            <div class="form-group">-->
            <!--                                <div class="select-wrap">-->
            <!--                                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>-->
            <!--                                    <select name="" id="" class="form-control ">-->
            <!--                                        <option class="dropdown_text_color" value="">Department</option>-->
            <!--                                        <option class="dropdown_text_color"  value="">Teeth Whitening</option>-->
            <!--                                        <option class="dropdown_text_color" value="">Teeth CLeaning</option>-->
            <!--                                        <option class="dropdown_text_color" value="">Quality Brackets</option>-->
            <!--                                        <option class="dropdown_text_color"  value="">Modern Anesthetic</option>-->
            <!--                                    </select>-->
            <!--                                </div>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <div class="col-sm-4">-->
            <!--                            <div class="form-group">-->
            <!--                                <div class="icon"><span class="icon-user"></span></div>-->
            <!--                                <input type="text" class="form-control" id="appointment_name" placeholder="Name">-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <div class="col-sm-4">-->
            <!--                            <div class="form-group">-->
            <!--                                <div class="icon"><span class="icon-paper-plane"></span></div>-->
            <!--                                <input type="text" class="form-control" id="appointment_email" placeholder="Email">-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                    <div class="row">-->
            <!--                        <div class="col-sm-4">-->
            <!--                            <div class="form-group">-->
            <!--                                <div class="icon"><span class="ion-ios-calendar"></span></div>-->
            <!--                                <input type="text" class="form-control appointment_date" placeholder="Date">-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <div class="col-sm-4">-->
            <!--                            <div class="form-group">-->
            <!--                                <div class="icon"><span class="ion-ios-clock"></span></div>-->
            <!--                                <input type="text" class="form-control appointment_time" placeholder="Time">-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <div class="col-sm-4">-->
            <!--                            <div class="form-group">-->
            <!--                                <div class="icon"><span class="icon-phone2"></span></div>-->
            <!--                                <input type="text" class="form-control" id="phone" placeholder="Phone">-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                    </div>-->
            <!---->
            <!--                    <div class="form-group">-->
            <!--                        <input type="submit" value="Make an Appointment" class="btn btn-primary">-->
            <!--                    </div>-->
            <!--                </form>-->
            <!--            </div>-->
        </div>
    </div>
</section>
<section class="ftco-section ftco-services">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-2">Our Service Keeps you Smile</h2>
                <p>"A smile is the prettiest thing you can wear" and we work towards to get you that with our quality
                    Dental treatment.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-tooth-1"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Teeth Whitening</h3>
                        <p>Whitening is among the most popular dental procedures because it can greatly improve how your
                            teeth look.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-dental-care"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Teeth Cleaning</h3>
                        <p>Teeth Cleaning is part of oral hygiene and involves the removal of dental plaque from teeth
                            (Dental caries)</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-tooth-with-braces"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Quality Brackets</h3>
                        <p>Orthodontics is the branch of dentistry that corrects teeth and jaws that are positioned
                            improperly.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services d-block text-center">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="flaticon-anesthesia"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">Modern Anesthetic</h3>
                        <p>Modern Anesthetic is a field of anesthesia that includes not only local but sedation and
                            general anesthesia.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-wrap mt-5">
        <div class="row d-flex no-gutters">
            <div class="col-md-12 d-flex">
                <div class="about-wrap">
                    <div class="container">
                        <div class="heading-section heading-section-white mb-5 ftco-animate">
                            <h2 class="mb-2">Clinic with a personal touch</h2>
                            <p>Personalized care is paramount and dental team must manage expectations. Even though we
                                live in a digitized world, offering a personal touch in your dental practice is still
                                important to your patients. </p>
                        </div>
                        <div class="list-services d-flex ftco-animate">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <span class="icon-check2"></span>
                            </div>
                            <div class="text">
                                <h3>Well Experience Dentist</h3>
                                <p>Dr. Adiba Ali is one of the best Dental Surgeon in Greater Noida & Noida for reasons
                                    that are sure to impress any patient. She has herself mastered the procedures as per
                                    international standards, which are followed in all dental surgeries at KOOL SMILES
                                    DENTAL to ensure painless dental procedures to keep patient discomfort to as close
                                    to zero as possible. Each one of Dr. Adiba's patients will vouch for this!</p>
                            </div>
                        </div>
                        <div class="list-services d-flex ftco-animate">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <span class="icon-check2"></span>
                            </div>
                            <div class="text">
                                <h3>High Technology Facilities</h3>
                                <p>Dr. Adiba Ali believes in using the latest techniques in dentistry to make your every
                                    visit to KOOL SMILES DENTAL a pleasurable experience. With her absolute focus on
                                    hygiene and sterilization, you can be sure that you are treated in the safest
                                    medical environment.</p>
                            </div>
                        </div>
                        <div class="list-services d-flex ftco-animate">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <span class="icon-check2"></span>
                            </div>
                            <div class="text">
                                <h3>Comfortable Clinics</h3>
                                <p>The brainchild of Dr. Adiba, KOOL SMILES DENTAL is the best Dental hospital in Noida.
                                    It brings you a variety of solutions for holistic dental care. KOOL SMILES DENTAL
                                    offers complex dental surgeries and cosmetic dentistry in an environment that puts
                                    you at complete ease. At KOOL SMILES DENTAL, you will feel like you have walked into
                                    a spa - only this one makes sure your teeth get a healthier lifestyle!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-3">Meet Our Experience Dentist</h2>
                <p>KOOL SMILES DENTAL is a literal example of quality dental care services in Greater Noida due to the
                    unique vision of its founder Dr. Adiba Ali, and it is her unmatched dedication to the cause of
                    spreading dental health awareness among the general public, that makes her a dentist par
                    excellence.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4"
                         style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/img/user1.jpeg);"></div>
                    <div class="info text-center">
                        <h3><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/dentist">Dr. ADIBA ALI</a></h3>
                        <span class="position">Dentist</span>
                        <div class="text">
                            <p>B.D.S, DH (USA), M.I.D.A<br>Dental Surgeon</p>
                            <ul class="ftco-social">
                                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4"
                         style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/img/user2.jpeg);"></div>
                    <div class="info text-center">
                        <h3><a href="#">Dr. AAS MOHAMMAD</a></h3>
                        <span class="position">Dentist</span>
                        <div class="text">
                            <p>B.D.S, M.I.D.A, P.G.C.O.I<br>Dental Surgeon & Certified Implantologist</p>
                            <ul class="ftco-social">
                                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4"
                         style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/img/user3.jpeg);"></div>
                    <div class="info text-center">
                        <h3><a href="#">Dr. ARUBA ALI</a></h3>
                        <span class="position">Dentist</span>
                        <div class="text">
                            <p>B.D.S, M.I.D.A</p>
                            <ul class="ftco-social">
                                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row  mt-5 justify-conten-center">
            <div class="col-md-12 ftco-animate">
                <p>Dr. Adiba has specialized in branches of dentistry which are concerned with the correction of
                    maligned, malposed crooked irregular teeth and replacement of missing teeth with dental implants.
                    Apart from this she has also taken advanced training in root canal treatment, crown bridgework,
                    aesthetic dentistry and children's dentistry in Noida. This makes her one of the few dentists in
                    Noida to be trained in various significant areas of dentistry.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-counter img" id="section-counter"
         style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/bg_1.jpg);"
         data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-md-3 aside-stretch py-5">
                <div class=" heading-section heading-section-white ftco-animate pr-md-4">
                    <h2 class="mb-3">Achievements</h2>
                    <!--                    <p>Kool Smiles Dental</p>-->
                </div>
            </div>
            <div class="col-md-9 py-5 pl-md-5">
                <div class="row">
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number" data-number="6">0</strong>
                                <span>Years of Experience</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number" data-number="600">0</strong>
                                <span>Qualified Dentist</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number" data-number="600">0</strong>
                                <span>Happy Smiling Customer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number" data-number="200">0</strong>
                                <span>Patients Per Year</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-3">Our Best Pricing</h2>
                <p>Our pricing are as per the industry standards and we always make sure that we remain competitive in
                    the market by offering great prices to our patients. </p>
            </div>
        </div>
        <!--        <div class="row">-->
        <!--            <div class="col-md-3 ftco-animate">-->
        <!--                <div class="pricing-entry pb-5 text-center">-->
        <!--                    <div>-->
        <!--                        <h3 class="mb-4">Basic</h3>-->
        <!--                        <p><span class="price">$24.50</span> <span class="per">/ session</span></p>-->
        <!--                    </div>-->
        <!--                    <ul>-->
        <!--                        <li>Diagnostic Services</li>-->
        <!--                        <li>Professional Consultation</li>-->
        <!--                        <li>Tooth Implants</li>-->
        <!--                        <li>Surgical Extractions</li>-->
        <!--                        <li>Teeth Whitening</li>-->
        <!--                    </ul>-->
        <!--                    <p class="button text-center"><a href="#" class="btn btn-primary btn-outline-primary px-4 py-3">Order now</a></p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <div class="col-md-3 ftco-animate">-->
        <!--                <div class="pricing-entry pb-5 text-center">-->
        <!--                    <div>-->
        <!--                        <h3 class="mb-4">Standard</h3>-->
        <!--                        <p><span class="price">$34.50</span> <span class="per">/ session</span></p>-->
        <!--                    </div>-->
        <!--                    <ul>-->
        <!--                        <li>Diagnostic Services</li>-->
        <!--                        <li>Professional Consultation</li>-->
        <!--                        <li>Tooth Implants</li>-->
        <!--                        <li>Surgical Extractions</li>-->
        <!--                        <li>Teeth Whitening</li>-->
        <!--                    </ul>-->
        <!--                    <p class="button text-center"><a href="#" class="btn btn-primary btn-outline-primary px-4 py-3">Order now</a></p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <div class="col-md-3 ftco-animate">-->
        <!--                <div class="pricing-entry active pb-5 text-center">-->
        <!--                    <div>-->
        <!--                        <h3 class="mb-4">Premium</h3>-->
        <!--                        <p><span class="price">$54.50</span> <span class="per">/ session</span></p>-->
        <!--                    </div>-->
        <!--                    <ul>-->
        <!--                        <li>Diagnostic Services</li>-->
        <!--                        <li>Professional Consultation</li>-->
        <!--                        <li>Tooth Implants</li>-->
        <!--                        <li>Surgical Extractions</li>-->
        <!--                        <li>Teeth Whitening</li>-->
        <!--                    </ul>-->
        <!--                    <p class="button text-center"><a href="#" class="btn btn-primary btn-outline-primary px-4 py-3">Order now</a></p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <div class="col-md-3 ftco-animate">-->
        <!--                <div class="pricing-entry pb-5 text-center">-->
        <!--                    <div>-->
        <!--                        <h3 class="mb-4">Platinum</h3>-->
        <!--                        <p><span class="price">$89.50</span> <span class="per">/ session</span></p>-->
        <!--                    </div>-->
        <!--                    <ul>-->
        <!--                        <li>Diagnostic Services</li>-->
        <!--                        <li>Professional Consultation</li>-->
        <!--                        <li>Tooth Implants</li>-->
        <!--                        <li>Surgical Extractions</li>-->
        <!--                        <li>Teeth Whitening</li>-->
        <!--                    </ul>-->
        <!--                    <p class="button text-center"><a href="#" class="btn btn-primary btn-outline-primary px-4 py-3">Order now</a></p>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
    </div>
</section>

<section class="ftco-section-parallax">
    <div class="parallax-img d-flex align-items-center">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                    <h2>Subcribe to our Newsletter</h2>
                    <div class="row d-flex justify-content-center mt-5">
                        <div class="col-md-8">
                            <form action="#" class="subscribe-form">
                                <div class="form-group d-flex">
                                    <input type="text" class="form-control" placeholder="Enter email address">
                                    <input type="submit" value="Subscribe" class="submit px-3">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2 class="mb-2">Testimony</h2>
                <span class="subheading">Our Happy Customer Says</span>
            </div>
        </div>
        <div class="row justify-content-center ftco-animate">
            <div class="col-md-8">
                <div class="carousel-testimony owl-carousel ftco-owl">
                    <div class="item">
                        <div class="testimony-wrap p-4 pb-5">
                            <div class="user-img mb-5"
                                 style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                            </div>
                            <div class="text text-center">
                                <p class="mb-5">Even the all-powerful Pointing has no control about the blind texts it
                                    is an almost unorthographic life One day however a small line of blind text by the
                                    name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
                                <p class="name">Dennis Green</p>
                                <span class="position">Marketing Manager</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap p-4 pb-5">
                            <div class="user-img mb-5"
                                 style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/person_2.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                            </div>
                            <div class="text text-center">
                                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                <p class="name">Dennis Green</p>
                                <span class="position">Interface Designer</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap p-4 pb-5">
                            <div class="user-img mb-5"
                                 style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/person_3.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                            </div>
                            <div class="text text-center">
                                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                <p class="name">Dennis Green</p>
                                <span class="position">UI Designer</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap p-4 pb-5">
                            <div class="user-img mb-5"
                                 style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                            </div>
                            <div class="text text-center">
                                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                <p class="name">Dennis Green</p>
                                <span class="position">Web Developer</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap p-4 pb-5">
                            <div class="user-img mb-5"
                                 style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                            </div>
                            <div class="text text-center">
                                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia
                                    and Consonantia, there live the blind texts.</p>
                                <p class="name">Dennis Green</p>
                                <span class="position">System Analytics</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-gallery">
    <div class="container-wrap">
        <div class="row no-gutters">
            <div class="col-md-3 ftco-animate">
                <a href="#" class="gallery img d-flex align-items-center"
                   style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/gallery-1.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ftco-animate">
                <a href="#" class="gallery img d-flex align-items-center"
                   style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/gallery-2.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ftco-animate">
                <a href="#" class="gallery img d-flex align-items-center"
                   style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/gallery-3.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ftco-animate">
                <a href="#" class="gallery img d-flex align-items-center"
                   style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/gallery-4.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!--<section class="ftco-section">-->
<!--    <div class="container">-->
<!--        <div class="row justify-content-center mb-5 pb-3">-->
<!--            <div class="col-md-7 text-center heading-section ftco-animate">-->
<!--                <h2 class="mb-2">Latest Blog</h2>-->
<!--                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-md-4 ftco-animate">-->
<!--                <div class="blog-entry">-->
<!--                    <a href="blog-single.html" class="block-20" style="background-image: url('images/image_1.jpg');">-->
<!--                    </a>-->
<!--                    <div class="text d-flex py-4">-->
<!--                        <div class="meta mb-3">-->
<!--                            <div><a href="#">Sep. 20, 2018</a></div>-->
<!--                            <div><a href="#">Admin</a></div>-->
<!--                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>-->
<!--                        </div>-->
<!--                        <div class="desc pl-3">-->
<!--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4 ftco-animate">-->
<!--                <div class="blog-entry" data-aos-delay="100">-->
<!--                    <a href="blog-single.html" class="block-20" style="background-image: url('images/image_2.jpg');">-->
<!--                    </a>-->
<!--                    <div class="text d-flex py-4">-->
<!--                        <div class="meta mb-3">-->
<!--                            <div><a href="#">Sep. 20, 2018</a></div>-->
<!--                            <div><a href="#">Admin</a></div>-->
<!--                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>-->
<!--                        </div>-->
<!--                        <div class="desc pl-3">-->
<!--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-4 ftco-animate">-->
<!--                <div class="blog-entry" data-aos-delay="200">-->
<!--                    <a href="blog-single.html" class="block-20" style="background-image: url('images/image_3.jpg');">-->
<!--                    </a>-->
<!--                    <div class="text d-flex py-4">-->
<!--                        <div class="meta mb-3">-->
<!--                            <div><a href="#">Sep. 20, 2018</a></div>-->
<!--                            <div><a href="#">Admin</a></div>-->
<!--                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>-->
<!--                        </div>-->
<!--                        <div class="desc pl-3">-->
<!--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<section class="ftco-quote">
    <div class="container">
        <div class="row">
            <div class="col-md-6 pr-md-5 aside-stretch py-5 choose">
                <div class="heading-section heading-section-white mb-5 ftco-animate">
                    <h2 class="mb-2">Services and Procedure we offer</h2>
                </div>
                <div class="ftco-animate">
                    <!--                    <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. Because there were thousands of bad Commas, wild Question Marks and devious Semikoli</p>-->
                    <ul class="un-styled my-5">
                        <li><span class="icon-check"></span>Dental X-Ray</li>
                        <li><span class="icon-check"></span>Tooth Colored Fillings</li>
                        <li><span class="icon-check"></span>Teeth Whitening(bleaching)</li>
                        <li><span class="icon-check"></span>Teeth Cleaning and Polishing</li>
                        <li><span class="icon-check"></span>Root Canal Treatment (RCT)</li>
                        <li><span class="icon-check"></span>Crown and Bridges (CAP)</li>
                        <li><span class="icon-check"></span>Dental Implant</li>
                        <li><span class="icon-check"></span>Child Tooth Treatment</li>
                        <li><span class="icon-check"></span>Full Mouth Rehabilitation</li>
                        <li><span class="icon-check"></span>Painless removal of teeth</li>
                        <li><span class="icon-check"></span>Orthodontic Braces</li>
                        <li><span class="icon-check"></span>Partial & Complete set of Denture</li>
                        <li><span class="icon-check"></span>Treatment of bad breath and bleeding gums</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 py-5 pl-md-5">
                <div class="heading-section mb-5 ftco-animate">
                    <h2 class="mb-2">Get In Touch</h2>
                </div>
                <form action="#" class="ftco-animate">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Full Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Website">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="" id="" cols="30" rows="7" class="form-control"
                                          placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" value="Get your query" class="btn btn-primary py-3 px-5">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>