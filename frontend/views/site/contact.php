<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="home-slider owl-carousel">
    <div class="slider-item bread-item" style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container" data-scrollax-parent="true">
            <div class="row slider-text align-items-end">
                <div class="col-md-7 col-sm-12 ftco-animate mb-5">
<!--                    <p class="breadcrumbs" data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span class="mr-2"><a href="#">Home</a></span> <span>Contact Us</span></p>-->
                    <h1 class="mb-3" data-scrollax=" properties: { translateY: '70%', opacity: .9}">Contact Us</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section contact-section ftco-degree-bg">
    <div class="container">

        <div class="row col-md-12">

            <div class="col-md-6">
                <div class="">
                    <h1 class="h1">Address</h1>
                    <br>
                </div>
                <div class="row d-flex mb-5 contact-info" style="margin-top: 20px; ">

                    <div class="w-100"></div>
                    <div class="col-md-12">
                        <p><span><b>Address:</b></span><br><a href="#">FF-118, First Floor, City Square Market, Ace City Noida Extension, Sector-01, Greater Noida, Uttar Pradesh 201318</a> </p>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <p><span><b>Phone:</b></span><br> <a href="tel://9810838463">	+91 9810838463<br>
                                +91 9811590343
                            </a></p>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <p><span><b>Email:</b></span><br> <a href="mailto:info@koolsmiles.in">info@koolsmiles.in</a></p>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <p><span><b>Website:</b></span><br> <a href="www.koolsmiles.in" target="_blank">koolsmiles.in</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="">
                    <h1 class="h1">Contact Us</h1>
                    <br>
                </div>
<!--                <div class="site-contact">-->
<!--                    <h1>--><?php //echo Html::encode($this->title) ?><!--</h1>-->
<!---->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-5">-->
                            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?php echo $form->errorSummary($model) ?>
                            <?php echo $form->field($model, 'name') ?>
                            <?php echo $form->field($model, 'email') ?>
                            <?php echo $form->field($model, 'subject') ?>
                            <?php echo $form->field($model, 'body')->textArea(['rows' => 6]) ?>
<!--                            --><?php //echo $form->field($model, 'verifyCode')->widget(Captcha::className(), [
//                                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
//                            ]) ?>
                            <div class="form-group">
                                <?php echo Html::submitButton(Yii::t('frontend', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
<!--                        </div>-->
<!--                    </div>-->

<!--                </div>-->

            </div>
            <div class="col-md-12" style="margin-top: 30px">
                <img class="d-block w-100"  style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/about3.jpeg" alt="Second slide">
            </div>



        </div>
    </div>
</section>


