<section class="home-slider owl-carousel">
    <div class="slider-item bread-item" style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container" data-scrollax-parent="true">
            <div class="row slider-text align-items-end">
                <div class="col-md-7 col-sm-12 ftco-animate mb-5">
<!--                    <p class="breadcrumbs" data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span class="mr-2"><a href="#">Home</a></span> <span>Blog</span></p>-->
                    <h1 class="mb-3" data-scrollax=" properties: { translateY: '70%', opacity: .9}">Read Our Blog</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 ftco-animate">
                        <div class="blog-entry">
                            <a href="#" class="block-20 blog-img" style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/img/blog.jpg);">
                            </a>
                            <div class="text d-flex py-4">
                                <div class="meta mb-3">
                                    <div><a href="#">July. 07, 2019</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 1</a></div>
                                </div>
                                <div class="desc pl-sm-3 pl-md-5">
                                    <h3 class="heading"><a href="#">Kool Smiles Dental</a></h3>
                                    <p>About Blog Koolsmiles.in is dedicated to improving oral health nationwide by making quality dental care affordable and accessible to everyone. Their mission is to enable people to live healthier, happier lives, one smile at a time. </p>
<!--                                    <p><a href="#" class="btn btn-primary btn-outline-primary">Read more</a></p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- END: col-md-8 -->

        </div>
    </div>
</section>