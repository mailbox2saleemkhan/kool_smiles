<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700" rel="stylesheet">
    <title>KOOL SMILES DENTAL - Dental, Best Dentist, Dental Surgeon</title>
    <meta name="description" content="KOOL SMILES DENTAL,dentist,offer more than what you might expect from the best dental clinic in Greater Noida offering Dental Implants, Root Canal Surgery (RCT)" />
    <?php $this->head() ?>
    <?php echo Html::csrfMetaTags() ?>
</head>
<body onload="initMap()">
<?php $this->beginBody() ?>
    <?php echo $content ?>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7SsD3CYq0hA1YEt8GH5sBENSS7fJ5VLg&libraries=places">
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
