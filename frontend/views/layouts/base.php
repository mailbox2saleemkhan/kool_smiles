<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="<?php echo Yii::getAlias("@frontendUrl") ?>">
            <!--            <img id="logo_img_id" class="logo_img" src="-->
            <?php //echo Yii::getAlias("@frontendUrl") ?><!--/img/logo.png"-->
            <!--                 alt="addriver logo">-->
            Kool <span>Smiles</span> Dental
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="<?php echo Yii::getAlias("@frontendUrl") ?>" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/about" class="nav-link">About</a></li>
                <li class="nav-item"><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/services" class="nav-link">Services</a></li>
<!--                <li class="nav-item"><a href="doctors.html" class="nav-link">Doctors</a></li>-->
                <li class="nav-item"><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/blog" class="nav-link">Blog</a></li>
                <li class="nav-item"><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/contact" class="nav-link">Contact</a></li>
                <?php

                if(!Yii::$app->user->getIsGuest()){
                    ?>
                    <li class="nav-item cta"><a href="<?php echo Yii::getAlias('@backendUrl') ?>" class="nav-link" ><span>Dashboard</span></a></li>
                    <?php
                }
                else{
                    ?>
                    <li class="nav-item cta"><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/user/sign-in/login" class="nav-link" ><span>Login</span></a></li>
                <?php
                }

                ?>

                <li class="nav-item cta"><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/appointment" class="nav-link" ><span>Make an Appointment</span></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->




<?php echo $content ?>

<div id="map"></div>

<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-4">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">KOOL SMILES DENTAL</h2>
                    <p>KOOL SMILES DENTAL is a literal example of quality dental care services in Greater Noida due to the unique vision of its founder Dr. Adiba Ali, and it is her unmatched dedication to the cause of spreading dental health awareness among the general public, that makes her the best dentist - Dental Surgeon in Greater Noida & a dentist par excellence.</p>
                </div>
                <ul class="ftco-footer-social list-unstyled float-md-left float-lft ">
                    <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                    <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h2 class="ftco-heading-2">Quick Links</h2>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/about" class="py-2 d-block">About</a></li>

                        <li><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/blog" class="py-2 d-block">Blog</a></li>
                        <li><a href="<?php echo Yii::getAlias("@frontendUrl") ?>/site/contact" class="py-2 d-block">Contact</a></li>
                    </ul>
                </div>
            </div>
            <!--            <div class="col-md-4 pr-md-4">-->
            <!--                <div class="ftco-footer-widget mb-4">-->
            <!--                    <h2 class="ftco-heading-2">Recent Blog</h2>-->
            <!--                    <div class="block-21 mb-4 d-flex">-->
            <!--                        <a class="blog-img mr-4" style="background-image: url(images/image_1.jpg);"></a>-->
            <!--                        <div class="text">-->
            <!--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>-->
            <!--                            <div class="meta">-->
            <!--                                <div><a href="#"><span class="icon-calendar"></span> Sept 15, 2018</a></div>-->
            <!--                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>-->
            <!--                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                    <div class="block-21 mb-4 d-flex">-->
            <!--                        <a class="blog-img mr-4" style="background-image: url(images/image_2.jpg);"></a>-->
            <!--                        <div class="text">-->
            <!--                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>-->
            <!--                            <div class="meta">-->
            <!--                                <div><a href="#"><span class="icon-calendar"></span> Sept 15, 2018</a></div>-->
            <!--                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>-->
            <!--                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--            </div>-->
            <div class="col-md-4">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Clinic</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">FF-118, First Floor, City Square Market, Ace City Noida Extension, Sector-01, Greater Noida, Uttar Pradesh 201318</span>
                            </li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">+91 9810838463 <br/>+91 9811590343</span></a>
                            </li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@koolsmiles.in</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p>
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script>
                    All rights reserved by Koolsmiles.in

                </p>
            </div>
        </div>
    </div>
</footer>


<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>



<?php $this->endContent() ?>
