<?php

namespace frontend\models;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('frontend', 'Name'),
            'email' => Yii::t('frontend', 'Email'),
            'subject' => Yii::t('frontend', 'Subject'),
            'body' => Yii::t('frontend', 'Body'),
            'verifyCode' => Yii::t('frontend', 'Verification Code')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()){
            $mail = new PHPMailer(true);
            try {
//                $mail->SMTPDebug = 2;                                     // Enable verbose debug output
                $mail->isSMTP();                                            // Set mailer to use SMTP
                $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
                $mail->SMTPAuth   = false;                                   // Enable SMTP authentication
                $mail->Username   = Yii::$app->params['adminEmail'];        // SMTP username
                $mail->Password   = '9560265727';                           // SMTP password
                $mail->Port       = 25;                                    // TCP port to connect to
                $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted

                //Recipients
                $mail->setFrom(Yii::$app->params['robotEmail'], 'Saleem Khan');
                $mail->addAddress(Yii::$app->params['adminEmail'], 'Saleem Khan'); // Add a recipient
//                $mail->addAddress('ellen@example.com');                    // Name is optional
                $mail->addReplyTo($this->email, $this->name);
//                $mail->addCC('cc@example.com');
//                $mail->addBCC('bcc@example.com');

                // Attachments
//                $mail->addAttachment('/var/tmp/file.tar.gz');              // Add attachments
//                $mail->addAttachment('/tmp/image.jpg', 'new.jpg');         // Optional name

                $mail->isHTML(true);                                         // Set email format to HTML
                $mail->Subject = $this->subject;
                $name = 'Name: '.$this->name;
                $email = '<br/>Email: '.$this->email;
                $thanks = '<br/><br/><br/>Thanks<br/>Kool Smiles Dental';
                $body = '<br/>'.$this->body;
                $mail->Body    = $name.$email.$body.$thanks;

                $mail->send();
                return true;
//                echo 'Message has been sent';
            } catch (Exception $e) {
//                print_r($e);
                return false;
//                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            }

//            return Yii::$app->mailer->compose()
//                ->setTo($email)
//                ->setFrom(Yii::$app->params['robotEmail'])
//                ->setReplyTo([$this->email => $this->name])
//                ->setSubject($this->subject)
//                ->setTextBody($this->body)
//                ->send();
        } else {
//            print_r($this->validate());
            return false;
        }
    }
}
