<?php
namespace frontend\controllers;

use frontend\models\Appointment;
use Yii;
use frontend\models\ContactForm;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionDentist()
    {
        return $this->render('dentist');
    }
    public function actionServices()
    {
        return $this->render('services');
    }


    public function actionBlog()
    {
        return $this->render('blog');
    }
    public function actionThanks()
    {
        return $this->render('thanks');
    }
    public function actionAppointment()
    {
        $model = new Appointment();
        if ($model->load(Yii::$app->request->post())) {

            $model->date = date("Y-m-d", strtotime($model->date));
            $model->time = date("H:i", strtotime($model->time));

            if ($model->save()) {
                /*email content ready*/
                $from =Yii::$app->params['robotEmail'];
                $to = Yii::$app->params['adminEmail'];
                $subject = 'Appointment confirmation';
                $msg = 'Dear Mr/Mrs/Ms,';
                $msg .= '<br/><br/>An appointment has been made for <strong>Dr Adiba Ali</strong>';
                $msg .= '<br/><br/>Patient name: '.$model->name;
                $msg .= '<br/>Patient number: '.$model->phone;
                $msg .= '<br/>Patient email: '.$model->email;
                $msg .= '<br/>Appointment date & time: '.$model->date.' '.$model->time;
                $msg .= '<br/><br/>'.$model->message;
                /*end*/
                if ($model->appointment($from,$to,$subject,$msg)) {             /*email sent*/

                    return $this->redirect(['site/thanks']);
                }
            }

        }
        return $this->render('appointment',[
            'model' => $model
        ]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
//                Yii::$app->getSession()->setFlash('alert', [
//                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
//                    'options'=>['class'=>'alert-success']
//                ]);
//                return $this->refresh();
                return $this->redirect(['site/thanks']);
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
        }

        return $this->render('contact', [
            'model' => $model
        ]);
    }
}
