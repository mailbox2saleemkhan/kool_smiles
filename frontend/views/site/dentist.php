<section class="home-slider owl-carousel">
    <div class="slider-item bread-item" style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container" data-scrollax-parent="true">
            <div class="row slider-text align-items-end">
                <div class="col-md-7 col-sm-12 ftco-animate mb-5">
<!--                    <p class="breadcrumbs" data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span class="mr-2"><a href="#">Home</a></span> <span></span></p>-->
                    <h1 class="mb-3" data-scrollax=" properties: { translateY: '70%', opacity: .9}">Dentist</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">

        <div class="row d-md-flex">
            <div class="col-md-6 "  >
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-item active">
                            <img class="d-block w-100" height="500px" style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/user1.jpeg" alt="1st slide">
                        </div>

                </div>

            </div>
            <div class="col-md-6 ftco-animate pr-md-5 order-md-first">
                <div class="row">
                    <div class="col-md-12 d-flex align-items-center">

                        <div class="tab-content ftco-animate" id="v-pills-tabContent">


                                    <h2 class="mb-4">Dr. ADIBA ALI</h2>
                            <h6 class="mb-4">B.D.S, DH (USA), M.I.D.A Dental Surgeon</h6>
                            <p>Dr. Adiba has specialized in branches of dentistry which are concerned with the correction of maligned, malposed crooked irregular teeth and replacement of missing teeth with dental implants. Apart from this she has also taken advanced training in root canal treatment, crown bridgework, aesthetic dentistry and children's dentistry in Noida. This makes her one of the few dentists in Noida to be trained in various significant areas of dentistry.</p>
                            <p>The brainchild of Dr. Adiba, KOOL SMILES DENTAL is the best Dental hospital in Noida. It brings you a variety of solutions for holistic dental care. KOOL SMILES DENTAL offers complex dental surgeries and cosmetic dentistry in an environment that puts you at complete ease. At KOOL SMILES DENTAL, you will feel like you have walked into a spa - only this one makes sure your teeth get a healthier lifestyle!</p>
                            <p>KOOL SMILES DENTAL has the distinction of being the most advanced dental, orthodontic and dental implant centre in Noida.
                                    </p>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>