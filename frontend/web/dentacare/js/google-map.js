var google;




// function init() {
// Initialize and add the map
function initMap() {
    // The location of Uluru
    var mylatlong = {lat: 28.5608038, lng: 77.4527549};
    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 13, center: mylatlong});
    // The marker, positioned at Uluru
    // var marker = new google.maps.Marker({position: uluru, map: map});

    var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h4 id="firstHeading" class="firstHeading">Kool Smiles Dental</h4>'+
        '<div id="bodyContent">'+
        '<p>FF-118, First Floor, City Square Market, Ace City Noida Extension, Sector-01, ' +
        'Greater Noida, Uttar Pradesh 201318'+
        '</p>'+
        '</div>'+
        '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 300
    });

    var marker = new google.maps.Marker({
        position: mylatlong,
        map: map,
        title: 'Kool Smiles Dental',
            // icon: {
            //     labelOrigin: new google.maps.Point(16,64),
            //     url: "https://drive.google.com/uc?id=0B3RD6FDNxXbdVXRhZHFnV2xaS1E"
            // },
            // label: {
            //     text: "Kool Smiles Dental",
            //     color: "Red",
            //     fontWeight: "bold",
            //     fontSize: "16px"
            // }
    });
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
}