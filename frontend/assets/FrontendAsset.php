<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'dentacare/css/open-iconic-bootstrap.min.css',
        'dentacare/css/animate.css',
        'dentacare/css/owl.carousel.min.css',
        'dentacare/css/owl.theme.default.min.css',
        'dentacare/css/magnific-popup.css',
        'dentacare/css/aos.css',
        'dentacare/css/ionicons.min.css',
        'dentacare/css/bootstrap-datepicker.css',
        'dentacare/css/jquery.timepicker.css',
        'dentacare/css/flaticon.css',
        'dentacare/css/icomoon.css',
        'dentacare/css/style.css',
        'css/style.css',
    ];

    public $js = [
        'dentacare/js/jquery.min.js',
        'dentacare/js/jquery-migrate-3.0.1.min.js',
        'dentacare/js/popper.min.js',
        'dentacare/js/bootstrap.min.js',
        'dentacare/js/jquery.easing.1.3.js',
        'dentacare/js/jquery.waypoints.min.js',
        'dentacare/js/jquery.stellar.min.js',
        'dentacare/js/owl.carousel.min.js',
        'dentacare/js/jquery.magnific-popup.min.js',
        'dentacare/js/aos.js',
        'dentacare/js/jquery.animateNumber.min.js',
        'dentacare/js/bootstrap-datepicker.js',
        'dentacare/js/jquery.timepicker.min.js',
        'dentacare/js/scrollax.min.js',
        'dentacare/js/google-map.js',
        'dentacare/js/main.js',
        'js/app.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//        'common\assets\Html5shiv',
    ];
}
