<section class="home-slider owl-carousel">
    <div class="slider-item bread-item" style="background-image: url(<?= getenv('FRONTEND_URL'); ?>/dentacare/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container" data-scrollax-parent="true">
            <div class="row slider-text align-items-end">
                <div class="col-md-7 col-sm-12 ftco-animate mb-5">
<!--                    <p class="breadcrumbs" data-scrollax=" properties: { translateY: '70%', opacity: 1.6}"><span class="mr-2"><a href="#">Home</a></span> <span>About</span></p>-->
                    <h1 class="mb-3" data-scrollax=" properties: { translateY: '70%', opacity: .9}">About Us - Providing the best Dental Care</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">

        <div class="row d-md-flex">
            <div class="col-md-6 "  >
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" height="500px" style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/about1.jpeg" alt="1st slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100"  height="500px"  style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/about2.jpeg" alt="2nd slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100"  height="500px"  style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/about4.jpeg" alt="3rd slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100"  height="500px"  style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/about5.jpeg" alt="4th slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100"  height="500px"  style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/about6.jpeg" alt="5th slide">
                        </div>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
            <div class="col-md-6 ftco-animate pr-md-5 order-md-first">
                <div class="row">
                    <div class="col-md-12 nav-link-wrap mb-5">
                        <div class="nav ftco-animate nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-whatwedo-tab" data-toggle="pill" href="#v-pills-whatwedo" role="tab" aria-controls="v-pills-whatwedo" aria-selected="true">What we do</a>

                            <a class="nav-link" id="v-pills-mission-tab" data-toggle="pill" href="#v-pills-mission" role="tab" aria-controls="v-pills-mission" aria-selected="false">Our mission</a>

                            <a class="nav-link" id="v-pills-goal-tab" data-toggle="pill" href="#v-pills-goal" role="tab" aria-controls="v-pills-goal" aria-selected="false">Our goal</a>
                        </div>
                    </div>
                    <div class="col-md-12 d-flex align-items-center">

                        <div class="tab-content ftco-animate" id="v-pills-tabContent">

                            <div class="tab-pane fade show active" id="v-pills-whatwedo" role="tabpanel" aria-labelledby="v-pills-whatwedo-tab">
                                <div>
<!--                                    <h2 class="mb-4">We Offer High Quality Services</h2>-->
                                    <p>KOOL SMILES DENTAL is a superspeciality dental clinic in Greater Noida Sector-01  (Delhi NCR region) with most advanced services with international standards for a better patient satisfaction.   We are fully digital and advanced clinic and we follow the latest industry standards. We share all your records including your treatment plan, invoices, receipts, treatment pictures, X-rays etc the moment treatments gets over. So that the patients can keep all your records always with them.</p>
                               <p>Our Dental Clinic provides the most up to date general, orthodontic and family dentistry.
Our Clinic has grown to provide a world class facility for the treatment of tooth loss, dental cosmetics and advanced restorative dentistry.</p>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-mission" role="tabpanel" aria-labelledby="v-pills-mission-tab">
                                <div>
<!--                                    <h2 class="mb-4">To Accomodate All Patients</h2>-->
                                    <p>We are the leading team in dentistry today. We started as three dedicated individuals who share the common ideal of being genuinely concerned with your overall health and self-esteem. We will provide you with a dazzling smile using the finest materials, the very latest in cutting-edge technology, and the most advanced skills and services. You will receive a truly remarkable, relaxing experience while we focus on your comfort.</p>

                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-goal" role="tabpanel" aria-labelledby="v-pills-goal-tab">
                                <div>
<!--                                    <h2 class="mb-4">Help Our Customers Needs</h2>-->
                                    <p>To act as a primary care provider for individuals and groups of patients. This includes providing emergency and multidisciplinary comprehensive oral health care, directing health promotion and disease prevention activities, and using advanced treatment modalities.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12" style="margin-top: 30px">
                <img class="d-block w-100"  style="object-fit: cover" src="<?php echo Yii::getAlias("@frontendUrl") ?>/img/about3.jpeg" alt="Second slide">
            </div>
        </div>
    </div>
</section>