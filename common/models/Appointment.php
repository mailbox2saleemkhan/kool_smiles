<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "appointment".
 *
 * @property integer $id
 * @property string $name
 * @property integer $phone
 * @property string $email
 * @property string $date
 * @property string $time
 * @property string $message
 */
class Appointment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'date', 'time'], 'required'],
            [['phone'], 'integer'],
            [['date', 'time'], 'safe'],
            [['message'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 255],
            ['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'date' => 'Date',
            'time' => 'Time',
            'message' => 'Message',
        ];
    }
}
